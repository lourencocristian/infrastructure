# ---------------------------------------------------------------------------------------------------------------------
# Manage DNS records configuration
# ---------------------------------------------------------------------------------------------------------------------

variable "Godaddy_customerID" {
  description = "Customer ID from Godaddy account. This variable is set in CI/CD variables under Gitlab project"
  type        = string
}

# DNS recors for principal domain
module "gabriellourenco_com" {

  source             = "./Godaddy"
  Godaddy_customerID = var.Godaddy_customerID
  domain_name        = "gabriellourenco.com"
  rootA              = "35.185.44.232"
  # Gitlab pages
  pages_txt_name = "_gitlab-pages-verification-code"
  pages_txt      = "gitlab-pages-verification-code=d82296a804653a03c3ac5de5410bec86"
  # Godaddy DNS servers
  nameservers = ["ns45.domaincontrol.com", "ns46.domaincontrol.com"]
}
