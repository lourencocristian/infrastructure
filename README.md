# Infrastructure 📡

Project to manage and automate my cloud infrastructure.

## Infrastructure diagram

A simple diagram for my personal infrastructure:

```plantuml
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/release/1-0/C4_Container.puml

Person_Ext(person, "Person", "Whoever o whatever requesting gabriellourenco.com")
Enterprise_Boundary(c1, "Cloud") {
  System(godaddy, "GoDaddy", "gabriellourenco.com Domain Name Server")
  System(gitlabpages, "GitlabPages", "gabriellourenco.com website")
}

Rel(person, godaddy, "Domain", "gabriellourenco.com")
Rel_Back(person, godaddy, "DNS", "IP Address")
Rel(person, gitlabpages, "Web", "gabriellourenco.com")
```

## Terraform (IaC)

Manage cloud infrastructure as a code with [**Terraform**](https://www.terraform.io/) and [**Gitlab CI/CD**](https://docs.gitlab.com/ee/user/infrastructure/iac/) to automate.

See: [**Terraform repo**](./Terraform/README.md)

## Contact

**Support:** [Gabriel Lourenco](https://gitlab.com/lourencocristian)
