# Terraform

Repository to store all **Terraform** code to manage my cloud infrastructure.

## Automation CI/CD 🚀️

With [Gitlab CI](.gitlab-ci.yml) the project **validate, test, build and deploy** infrastructure all automated.

### 1. Validate

Using `terraform validate` command the **validate job** check the configuration files.

### 2. Test

Using [Gitlab SAST-IaC](https://docs.gitlab.com/ee/user/application_security/iac_scanning/) for static code analysis of infrastructure as code to identify vulnerabilities. The JSON report file can be downloaded from the CI pipelines page, or the pipelines tab on merge requests.

### 3. Build

Using `terraform plan` command the **build job** create an execution plan to review what changes are planed to deploy.

### 4. Deploy

Usign `terraform apply` command the **deploy job** create the infrastructure defined in the Terraform repository. This job is create only from main branch and is manual execution. Find it in [manual jobs](https://gitlab.com/lourencocristian/infrastructure/-/pipelines?page=1&scope=all&status=manual).

### Backend

The **Terraform** backend state is stored in Gitlab using [**Managed Terraform state backend**](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html), the configuration is so simple: [**backend.tf**](backend.tf).

## DNS 🌐

Manage **DNS** records for domains.

In [**DNS.tf**](dns.tf) file are defined variables to manage the domain. In my case, I use **Godaddy** to store my domains and as a DNS server. 

See: [**Godaddy module**](./Godaddy/README.md).
