# Godaddy - DNS 🌐

Manage DNS records for domains stored in [**Godaddy**](https://dcc.godaddy.com/).

This module use a provider [***Github - n3integration***](https://github.com/n3integration/terraform-provider-godaddy), see config in [**provider.tf**](./Godaddy/provider.tf).

## Credentials

The provider required access to **Godaddy API** to manage domain records, find it in: [developer Godaddy](https://developer.godaddy.com/).

Those credentials are set in [Gitlab CI/CD variables section](https://gitlab.com/lourencocristian/infrastructure/-/settings/ci_cd):

- `TF_VAR_Godaddy_customerID`: Godaddy ID customer account.
- `GODADDY_API_KEY`: identifier.
- `GODADDY_API_SECRET`: secret key.

**API Key development (OTE):**

For testing an review tasks you can use development environment, there is my OTE credentials:

- `GODADDY_API_KEY`: `3mM44UbhTViuix_TycoYyoY9cqc1r1LbnUdnL`.
- `GODADDY_API_SECRET`: `GMhVpKzgabPXb2sSNenSfm`.

## Documentation

For reference see [**Godaddy API docs**](https://developer.godaddy.com/getstarted).
