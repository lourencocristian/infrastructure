# ---------------------------------------------------------------------------------------------------------------------
# Create DNS records for a simple domain under Godaddy account
# ---------------------------------------------------------------------------------------------------------------------
variable "domain_name" {
  type = string
}
variable "Godaddy_customerID" {
  description = "Customer ID from Godaddy account. This variable is set in CI/CD Variables under Gitlab project"
  type        = string
}
variable "pages_txt_name" {
  description = "Name for Gitlab pages"
  type        = string
}
variable "pages_txt" {
  description = "Verification for Gitlab pages"
  type        = string
}
variable "rootA" {
  description = "Principal A record for @"
  type        = string
}
variable "nameservers" {
  description = "Specify nameservers for your domain"
  type        = list(string)
}

resource "godaddy_domain_record" "gd-domain" {
  domain = var.domain_name

  // required if provider key does not belong to customer
  customer = var.Godaddy_customerID

  // A record block allows you to configure A, or NS records with a custom time-to-live value. A record block also allow you to configure AAAA, CNAME, TXT, or MX records
  // Regular www CNAME
  record {
    name = "www"
    type = "CNAME"
    data = var.domain_name
    ttl  = 3600
  }

  // TXT use to validate
  record {
    name = var.pages_txt_name
    type = "TXT"
    data = var.pages_txt
    ttl  = 3600
  }

  // Main A record use for principal Web Site 
  record {
    name = "@"
    type = "A"
    data = var.rootA
    ttl  = 3600
  }
  // specify any A records associated with the domain
  // addresses   = ["192.168.1.2", "192.168.1.3"]

  // specify any custom nameservers for your domain
  // note: godaddy now requires that the 'custom' nameservers are first supplied through the ui
  nameservers = var.nameservers
}
